package ch.heiafr.tic.lorisruinet.tp01_diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageView vDice; //virtual die 1
    private ImageView vDice2; //virtual die 2
    private final int DICE_FACES = 6; //number of faces on each die
    private final Random random = new Random();
    static private boolean multipleDice=true; //true if 2 dice are rolled

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vDice = findViewById(R.id.imgViewDice);  //assign visual die 1 to object
        vDice2 = findViewById(R.id.imgViewDice2);  //assign visual die 2 to object
        Button roll = findViewById(R.id.btnRoll);  //assign visual roll btnRoll to object
        Button die1 = findViewById(R.id.btn1Die);  //btnRoll with "1" text
        Button dice2 = findViewById(R.id.btn2Dice);  // btnRoll with "2" text

        //set roll btnRoll functionality
        roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                //roll die 1
                int value = random.nextInt(DICE_FACES) + 1;
                switch(value){
                    case 1:
                        vDice.setImageResource(R.drawable.dice_1);
                        Log.i("rng1","1");
                        break;
                    case 2:
                        vDice.setImageResource(R.drawable.dice_2);
                        Log.i("rng1","2");
                        break;
                    case 3:
                        vDice.setImageResource(R.drawable.dice_3);
                        Log.i("rng1","3");
                        break;
                    case 4:
                        vDice.setImageResource(R.drawable.dice_4);
                        Log.i("rng1","4");
                        break;
                    case 5:
                        vDice.setImageResource(R.drawable.dice_5);
                        Log.i("rng1","5");
                        break;
                    case 6:
                        vDice.setImageResource(R.drawable.dice_6);
                        Log.i("rng1","6");
                        break;
                    default:
                        vDice.setImageResource(R.drawable.dice_empty);
                        Log.e("rng1","unexpected result");
                }

                //roll die 2 if necessary
                if(!multipleDice) return;
                value = random.nextInt(DICE_FACES)+1;
                switch(value){
                    case 1:
                        vDice2.setImageResource(R.drawable.dice_1);
                        Log.i("rng2","1");
                        break;
                    case 2:
                        vDice2.setImageResource(R.drawable.dice_2);
                        Log.i("rng2","2");
                        break;
                    case 3:
                        vDice2.setImageResource(R.drawable.dice_3);
                        Log.i("rng2","3");
                        break;
                    case 4:
                        vDice2.setImageResource(R.drawable.dice_4);
                        Log.i("rng2","4");
                        break;
                    case 5:
                        vDice2.setImageResource(R.drawable.dice_5);
                        Log.i("rng2","5");
                        break;
                    case 6:
                        vDice2.setImageResource(R.drawable.dice_6);
                        Log.i("rng2","6");
                        break;
                    default:
                        vDice2.setImageResource(R.drawable.dice_empty);
                        Log.e("rng2","unexpected result");
                }

            }
        });

        // set btn1Die functionality
        die1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!multipleDice)return;
                multipleDice = false;
                vDice2.setVisibility(View.GONE);
                Log.i("layout","Remove die 2");
            }
        });

        // set btn2Dice functionality
        dice2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(multipleDice)return;
                multipleDice = true;
                vDice2.setVisibility(View.VISIBLE);
                vDice2.setImageResource(R.drawable.dice_1);
                Log.i("layout","Add die 2");
            }
        });
    }
}